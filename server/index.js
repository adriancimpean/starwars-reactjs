const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const port = 8000;

let messages = [];
let users = [];

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.json(messages);
});

app.post("/contact", (req, res) => {
  const message = req.body;
  messages.push(message);
  console.log(message);

  res.send("Succesfully submited");
});

app.post("/register", (req, res) => {
  console.log("registering new user...");
  const user = req.body;
  let userAlreadyRegistered = false;

  users.map((existingUser) => {
    if (existingUser.email === user.email) {
      userAlreadyRegistered = true;
    }
  });

  if (userAlreadyRegistered) {
    res.send({
      error: "User already existing",
    });
    return;
  }

  users.push(user);
  res.status(201);
  res.json({
    user: user,
  });
  console.log(users);
});

app.post("/login", (req, res) => {
  const user = req.body;
  console.log("tried to login");

  users.map((existingUser) => {
    if (
      existingUser.email === user.email &&
      existingUser.password === user.password
    ) {
      res.send("token123");
      return;
    }
  });
});

app.listen(port, () => console.log(`listening on port ${port}`));
