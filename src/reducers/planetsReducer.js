import _ from "lodash";
import * as types from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_PLANETS:
      return { ...state, ..._.mapKeys(action.payload.results, "name") };
    case types.FETCH_PLANET:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
