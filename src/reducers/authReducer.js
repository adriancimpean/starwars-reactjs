import { LOGIN, REGISTER } from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case LOGIN:
      console.log(action.payload);
      return { ...state, token: action.payload, isLoggedIn: true };
    case REGISTER:
      return action.payload;
    default:
      return state;
  }
};
