import * as types from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_FILM_NAME:
      return { ...state, [action.payload.url]: action.payload };
    case types.FETCH_FILM:
        return { ...state, ...action.payload }
    default:
      return state;
  }
};
