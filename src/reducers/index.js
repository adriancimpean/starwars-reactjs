import { combineReducers } from "redux";
import starshipsReducer from "./starshipsReducer";
import peopleReducer from "./peopleReducer";
import planetsReducer from "./planetsReducer";
import filmReducer from "./filmReducer";
import authReducer from "./authReducer";

export default combineReducers({
  starships: starshipsReducer,
  people: peopleReducer,
  planets: planetsReducer,
  films: filmReducer,
  auth: authReducer
});
