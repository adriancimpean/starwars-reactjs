import _ from "lodash";
import * as types from "../actions/types"

export default (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_PEOPLE:
      return { ...state, ..._.mapKeys(action.payload.results, "name") };
    case types.FETCH_CHARACTER:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
