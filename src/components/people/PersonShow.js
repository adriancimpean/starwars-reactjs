import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchCharacter } from "../../actions";
import { 
  Typography,
  Button,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core"
import { makeStyles } from "@material-ui/styles"
import FilmName from "../names/FilmName";
import Header from "../Header";

const useStyles = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
  },
  listItem: {
    flex: '0.15',
    minWidth: '100px'
  },
  backButton: {
    marginTop: 15,
  }
}))

const  PersonShow = (props) => {
  const classes = useStyles();

  useEffect(() => {
    const { id } = props.match.params;

    props.fetchCharacter(id);
  }, [])

  const renderFilmName = () => {
    if (props.person.films !== undefined) {
      return props.person.films.map((film) => {
        const filmId =
          film.length === 29
            ? film.charAt(27)
            : film.charAt(27) + film.charAt(30);

        return (
          <div>
            <List>
            <Typography variant="body2" >
              <FilmName key={filmId} url={film} />
            </Typography>
            </List>
          </div>
        );
      });
    }
  }

  const person = props.person;

  const renderPersonDetails = () => {
    return (
      <List>
        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Height" />
          <ListItemText className={classes.listItem} primary={person.height} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Mass" />
          <ListItemText className={classes.listItem} primary={person.mass} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Hair color" />
          <ListItemText className={classes.listItem} primary={person.hair_color} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Skin color" />
          <ListItemText className={classes.listItem} primary={person.skin_color} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Eye color" />
          <ListItemText className={classes.listItem} primary={person.eye_color} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Birth year" />
          <ListItemText className={classes.listItem} primary={person.birth_year} />
        </ListItem>
      </List>
    )
  }

  return (
    <div>
      <Header title={person.name} />
      <Button className={classes.backButton} variant="contained" color="primary" component={Link} to="/people">
        Back
      </Button>
      <Typography variant="h2" className={classes.title}>
        {person.name}
      </Typography>
      <Typography variant="h4">
        Information:
      </Typography>
        {renderPersonDetails()}
      <Typography variant="h5">
        Films:
      </Typography>
      {renderFilmName()}      
    </div>
  );
}

const mapStateToProps = (state) => {
  return { person: state.people };
};

export default connect(mapStateToProps, { fetchCharacter })(PersonShow);
