import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchPeople } from "../../actions";
import CardList from "../CardList";
import {
  Container,
  Grid,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../Header";

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
}))

const PeopleList = (props) => {
  const classes = useStyles();

  useEffect(() => {
    props.fetchPeople();
  })

  const renderList = () => {
    if (!props.people) {
      return <div>Loading...</div>;
    }

    return props.people.slice(0, 10).map((person) => {
      const personId =
        person.url.length === 30
          ? person.url.charAt(28)
          : person.url.charAt(28) + person.url.charAt(29);

      return (
        <Grid item key={personId} xs={12} sm={6} md={4}>
          <CardList key={personId} title={person.name} id={personId} route="person" />
        </Grid>
      );
    });
  }

  return (
    <div>
      <Header title="Characters"/>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          {renderList()}
        </Grid>
      </Container>    
    </div>  
  )
}

const mapStateToProps = (state) => {
  return { people: Object.values(state.people) };
};

export default connect(mapStateToProps, { fetchPeople })(PeopleList);
