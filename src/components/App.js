import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import Login from "./auth/Login";
import Register from "./auth/Register";
import history from "../history";
import Home from "./Home"
import Contact from "./contact/Contact"
import StarshipsList from "./starship/StarshipsList";
import StarshipShow from "./starship/StarshipShow";
import PlanetsList from "./planets/PlanetsList";
import PlanetShow from "./planets/PlanetShow";
import PeopleList from "./people/PeopleList"
import PersonShow from "./people/PersonShow"

const App = () => {
  return (
    <div>
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/register" exact component={Register} />
          <Route path="/home" exact component={Home} />
          <Route path="/contact" exact component={Contact} />
          <Route path="/starships" exact component={StarshipsList} />
          <Route path="/starships/:id" exact component={StarshipShow} />
          <Route path="/planets" exact component={PlanetsList} />
          <Route path="/planets/:id" exact component={PlanetShow} /> 
          <Route path="/people" exact component={PeopleList} />
          <Route path="/person/:id" exact component={PersonShow} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
