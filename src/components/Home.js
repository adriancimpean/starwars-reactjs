import React from "react";
import { Link } from "react-router-dom";
import {
  AppBar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  CssBaseline,
  Grid,
  Toolbar,
  Typography,
  Container,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LocalActivityIcon from '@material-ui/icons/LocalActivity';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%",
  },
  cardContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const Home = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <LocalActivityIcon className={classes.icon} />
          <Typography variant="h6" color="inherit" noWrap style={{flex: 1}}>
            Star Wars
          </Typography>
          <Button color="inherit" component={Link} to="/contact">
            Contact
          </Button>
        </Toolbar>
      </AppBar>
      <main>
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Star Wars
            </Typography>
            <Typography
              variant="h5"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Star Wars is an American epic space opera media franchise created
              by George Lucas, which began with the eponymous 1977 film and
              quickly became a worldwide pop-culture phenomenon.
            </Typography>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            <Grid item key="starships" xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://i.ibb.co/1sCZnqw/starship.jpg"
                  title="starshipImage"
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    Starships
                  </Typography>
                  <Typography>
                    See a list of starships from your favorite movie!
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" color="primary" component={Link} to="/starships">
                    View
                  </Button>
                </CardActions>
              </Card>
            </Grid>

            <Grid item key="people" xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://i.ibb.co/2vqHmLN/clonewarsyodasacrifice.jpg"
                  title="peopleImage"
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    Characters
                  </Typography>
                  <Typography>
                    Get to know more about your favorite character!
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" color="primary" component={Link} to="/people">
                    View
                  </Button>
                </CardActions>
              </Card>
            </Grid>

            <Grid item key="Planets" xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://i.ibb.co/VWSKkhp/mustafar-tall.jpg"
                  title="planetImage"
                />
                <CardContent className={classes.cardContent}>
                  <Typography gutterBottom variant="h5" component="h2">
                    Planets
                  </Typography>
                  <Typography>
                    Do you want to travel?
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small" color="primary" component={Link} to="/planets">
                    View
                  </Button>
                </CardActions>
              </Card>
            </Grid>

          </Grid>
        </Container>
      </main>
    </React.Fragment>
  );
};

export default Home;
