import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchPlanets } from "../../actions";
import CardList from "../CardList";
import {
  Container,
  Grid,
  CircularProgress
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../Header";


const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
}))

const PlanetsList = (props) => {
  const classes = useStyles()

  useEffect(() => {
    props.fetchPlanets();
  })

  const renderList = () => {
    return props.planets.slice(0, 10).map((planet) => {
      const planetId =
        planet.url.length === 31
          ? planet.url.charAt(29)
          : planet.url.charAt(29) + planet.url.charAt(30);

      return (
        <Grid item key={planetId} xs={12} sm={6} md={4}>
          <CardList key={planetId} title={planet.name} id={planetId} route="planets" />
        </Grid>
      );
    });
  }

  return(
    <div>
      <Header title="Planets"/>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          {renderList()}
        </Grid>
      </Container>    
    </div>  
  )
}

const mapStateToProps = (state) => {
  return { planets: Object.values(state.planets) };
};

export default connect(mapStateToProps, { fetchPlanets })(PlanetsList);
