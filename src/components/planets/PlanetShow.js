import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchPlanet } from "../../actions";
import { Link } from "react-router-dom";
import Header from "../Header";
import { 
  Typography,
  Button,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core"
import { makeStyles } from "@material-ui/styles"

const PlanetShow = (props) => {
  useEffect(() => {
    const { id } = props.match.params;
    props.fetchPlanet(id); 
  }, [])

  const useStyles = makeStyles((theme) => ({
    title: {
      textAlign: 'center',
    },
    listItem: {
      flex: '0.15',
      minWidth: '100px'
    },
  }))

  const planet = props.planet;

  const classes = useStyles();

  const renderPlanetDetails = () => {
    return (
      <List>
        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Population" />
          <ListItemText className={classes.listItem} primary={planet.population} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Diameter" />
          <ListItemText className={classes.listItem} primary={planet.diameter} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Climate" />
          <ListItemText className={classes.listItem} primary={planet.climate} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Rotation Period" />
          <ListItemText className={classes.listItem} primary={planet.rotation_period} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Orbital period" />
          <ListItemText className={classes.listItem} primary={planet.orbital_period} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Gravity" />
          <ListItemText className={classes.listItem} primary={planet.gravity} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Terrain" />
          <ListItemText className={classes.listItem} primary={planet.terrain} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Surface water" />
          <ListItemText className={classes.listItem} primary={planet.surface_water} />
        </ListItem>
      </List>
    )
  }

  return (
    <div>
      <Header title={planet.name} />
      <Typography variant="h3" className={classes.title}>
        {planet.name}
      </Typography>
      {renderPlanetDetails()}
      <Button color="primary" variant="contained" component={Link} to="/planets">
                Back
      </Button>
    </div>
  )
}

const mapStateToProps = (state) => {
  return { planet: state.planets };
};

export default connect(mapStateToProps, { fetchPlanet })(PlanetShow);
