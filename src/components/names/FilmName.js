import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom"
import { fetchFilmName } from "../../actions";
import { 
  CircularProgress,
  ListItem,
  ListItemText
} from "@material-ui/core"

const FilmName = (props) => {

  useEffect(() => {
    props.fetchFilmName(props.url);
  }, [])

  const renderFilm = () => {
    if (!props.films) {
      return <CircularProgress />
    }
    return (
      <ListItem divider alignItems="flex-start">
        <ListItemText primary={props.films.title}/>
      </ListItem>
    )
  }

  return (
    <div>{renderFilm()}</div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return { films: state.films[ownProps.url] };
};

export default connect(mapStateToProps, { fetchFilmName })(FilmName);
