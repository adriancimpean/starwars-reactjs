import React from "react";
import { 
    AppBar,
    Typography,
    Button,
    Toolbar
 } from "@material-ui/core";
 import { makeStyles } from "@material-ui/core/styles";
 import LocalActivityIcon from "@material-ui/icons/LocalActivity";
 import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    homeButton: {
        marginRight: theme.spacing(5),
    }
}))

const Header = ({title}) => {
    const classes = useStyles();

    return (
        <AppBar position="relative">
            <Toolbar>
            <LocalActivityIcon className={classes.icon} />
            <Button color="inherit" className={classes.homeButton} variant="outlined" component={Link} to="/home">
                Home
            </Button>
            <Typography variant="h6" color="inherit" noWrap style={{flex: 1}}>
                {title}
            </Typography>
            <Button color="inherit" component={Link} to="/contact">
                Contact
            </Button>
            </Toolbar>
        </AppBar>
    )
}

export default Header;