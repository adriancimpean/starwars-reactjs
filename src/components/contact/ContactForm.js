import React from "react";
import { Field, reduxForm } from "redux-form";
import formField from "./formField";

const ContactForm = (props) => {
  return (
    <div>
      <form className="ui form error" onSubmit={props.handleSubmit}>
        <div className="field">
          <Field
            type="text"
            label="First Name"
            name="firstName"
            component={formField}
            placeholder="First Name"
          />
        </div>
        <div className="field">
          <Field
            type="text"
            label="Last Name"
            name="lastName"
            component={formField}
            placeholder="Last Name"
          />
        </div>
        <div className="field">
          <Field
            label="Birth Date"
            type="date"
            component={formField}
            name="birthDate"
          />
        </div>
        <div className="field">
          <Field
            type="email"
            label="Email"
            name="email"
            component={formField}
            placeholder="E-mail"
          />
        </div>
        <div className="field">
          <Field
            label="Your messaage:"
            component={formField}
            type="textarea"
            name="message"
          />
        </div>
        <button className="ui big fluid circular green button">
          Send us your thoughts!
        </button>
      </form>
    </div>
  );
};

function validate(values) {
  const errors = {};

  if (!values.firstName) {
    errors.firstName = "Please enter your first name ";
  }
  if (!values.lastName) {
    errors.lastName = "Please enter your last name";
  }
  if (!values.email) {
    errors.email = "Please enter your email";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Please enter a valid email address";
  }
  if (!values.message) {
    errors.message = "Please enter your message";
  }

  return errors;
}

export default reduxForm({
  validate,
  form: "contactForm"
})(ContactForm);
