import React from "react";
import ContactForm from "./ContactForm";
import axios from "axios";

class Contact extends React.Component {
  onSubmit = async (values) => {
    console.log(values);
    const response = await axios.post("http://localhost:8000/contact", values);
    alert(response.data);
  };

  render() {
    return <ContactForm onSubmit={this.onSubmit} />;
  }
}

export default Contact;
