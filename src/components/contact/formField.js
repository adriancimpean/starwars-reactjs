import React from "react";

export default ({input, label, meta: {error, touched}}) => {
    return(
        <div>
            <label>{label}</label>
            <input {...input} />
            <div className="ui error message">
                {touched && error}
            </div>
        </div>
    )
}