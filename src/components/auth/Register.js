import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../../actions/authActions";
import { Link } from "react-router-dom"
import history from "../../history";
import {
  Avatar,
  Button,
  CssBaseline,
  TextField,
  Grid,
  Typography,
  Container,
  Snackbar,
} from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles";
import LockedOutIcon from "@material-ui/icons/LockOutlined";
import Alert from '@material-ui/lab/Alert';

const Register = () => {
  const [open, setOpen] = React.useState(false);

  const dispatch = useDispatch();

  const onSubmit = async (event) => {
    event.preventDefault();
    event.stopPropagation();

    if (
      event.target.password.value !== event.target.passwordConfirmation.value
    ) {
      alert("Passwords do not match")
      return;
    }

    const user = {
      email: event.target.email.value,
      password: event.target.password.value,
    };

    dispatch(register(user));
    setOpen(true);
  };

  useSelector((state) => console.log(state));
  let result = useSelector((state) => state.auth);
  
  const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%',
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3,0,2)
    }
  }))

  const classes = useStyles()

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockedOutIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField 
                autoComplete="email"
                name="email"
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email"
                type="email"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField 
                variant="outlined"
                required
                fullWidth
                id="password"
                label="Password"
                type="password"
                name="password"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField 
                variant="outlined"
                required
                fullWidth
                id="confirmPassword"
                label="Confirm password"
                type="password"
                name="passwordConfirmation"
              />
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign up
            </Button>

            <Button
              component={Link}
              to="/"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
            >
              Back
            </Button>
          </Grid>
        </form>
      </div>
      
      <div>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={result.error == undefined ? "success" : "error"}>
          {result.error == undefined ? "Successfully Registered!" : result.error}
        </Alert>
      </Snackbar>
    </div>
    </Container>

    
  );
};

export default Register;
