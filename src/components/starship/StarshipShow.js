import React, { useEffect } from "react";
import clsx from "clsx";
import { connect } from "react-redux";
import { fetchStarship } from "../../actions";
import {
  Container,
  Grid,
  Paper,
  Button,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../Header"
import Price from "./Price";
import StarshipName from "./StarshipName";
import { Link } from "react-router-dom"

const StarshipShow = (props) => {
  useEffect(() => {
    const { id } = props.match.params;
    props.fetchStarship(id);
  }, []);
  
  const starship = props.starship;

  const useStyles = makeStyles((theme) => ({
    appBarSpacer: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      height: '100vh',
      overflow: 'auto',
    },
    container: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
    },
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
    },
    fixedHeight: {
      height: 240,
    },
    listItem: {
      flex: '0.2',
      minWidth: '100px'
    },
  }));

  const classes = useStyles()
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  const renderStarshipDetails = () => {
    return(
      <List>
        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Length" />
          <ListItemText className={classes.listItem} primary={starship.length} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Crew" />
          <ListItemText className={classes.listItem} primary={starship.crew} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Passengers" />
          <ListItemText className={classes.listItem} primary={starship.passengers} />
        </ListItem>

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Hyperdrive Rating" />
          <ListItemText className={classes.listItem} primary={starship.hyperdrive_rating} />
        </ListItem> 

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Max speed" />
          <ListItemText className={classes.listItem} primary={starship.max_atmosphering_speed} />
        </ListItem> 

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Cargo capacity" />
          <ListItemText className={classes.listItem} primary={starship.cargo_capacity} />
        </ListItem> 

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Consumables change interval" />
          <ListItemText className={classes.listItem} primary={starship.consumables} />
        </ListItem> 

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="MGLT" />
          <ListItemText className={classes.listItem} primary={starship.MGLT} />
        </ListItem> 

        <ListItem  divider alignItems="flex-start">
          <ListItemText className={classes.listItem} primary="Starship Class" />
          <ListItemText className={classes.listItem} primary={starship.starship_class} />
        </ListItem> 

    </List>
    );
  }

  return (
    <div>
      <Header title={starship.name}/>
      <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container maxWidth="lg" className={classes.container}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={8} lg={9}>
                <Paper className={fixedHeightPaper}>
                  <StarshipName manufacturer={starship.manufacturer} name={starship.name} model={starship.model} />
                </Paper>
              </Grid>
              <Grid item xs={12} md={4} lg={3}>
                <Paper className={fixedHeightPaper}>
                  <Price price={starship.cost_in_credits}/>
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  {renderStarshipDetails()}
                </Paper>
              </Grid>
            </Grid>
          </Container>
          <Button color="primary" variant="contained" component={Link} to="/starships">
                Back
          </Button>
      </main>
    </div>
  )
}

const mapStateToProps = (state) => {
  return { starship: state.starships };
};

export default connect(mapStateToProps, { fetchStarship })(StarshipShow);
