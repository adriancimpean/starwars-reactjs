import React from 'react';
import { Typography, Button } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles({
    price: {
      flex: 1,
    },
  });

const Price = ({price}) => {
    const classes = useStyles() 
  return (
    <React.Fragment>
    <Typography color="textSecondary">
        Starship price
      </Typography>
      <Typography component="p" variant="h4" className={classes.price}>
        ${price}
      </Typography>

    <div>
      <Button color="primary" variant="contained">
          Buy now
      </Button>
    </div>
    </React.Fragment>
  );
}

export default Price;