import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchStarships } from "../../actions";
import CardList from "../CardList";
import {
  Container,
  Grid,
  CircularProgress
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../Header";

const useStyles = makeStyles((theme) => ({
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
}))

const StarshipsList = (props) => {
  const classes = useStyles()

  useEffect(() => {
    props.fetchStarships();
  }, [])

  const renderList = () => {
    if (props.starships === undefined || props.starships === null) {
      return <CircularProgress />;
    }

    return props.starships.slice(0, 10).map((starship) => {
      const starshipId =
        starship.url.length === 33
          ? starship.url.charAt(31)
          : starship.url.charAt(31) + starship.url.charAt(32);

      return (
        <Grid item key={starshipId} xs={12} sm={6} md={4}>
          <CardList key={starshipId} title={starship.name} description={starship.model} id={starshipId} route="starships" />
        </Grid>
      );
    });
  }

  if(!props.starships) {
    return <CircularProgress />;
  }
  
  return(
    <div>
      <Header title="Starships"/>
      <Container className={classes.cardGrid} maxWidth="md">
        <Grid container spacing={4}>
          {renderList()}
        </Grid>
      </Container>    
    </div>
  )
}

const mapStateToProps = (state) => {
  return { starships: Object.values(state.starships) };
};

export default connect(mapStateToProps, { fetchStarships })(StarshipsList);
