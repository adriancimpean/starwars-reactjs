import React from 'react';
import Typography from '@material-ui/core/Typography';


const StarshipName = ({manufacturer, name, model}) => {
  return (
    <React.Fragment>
    <Typography color="textSecondary">
       {manufacturer}
      </Typography>
      <Typography component="p" variant="h4">
        {name}
      </Typography>
      <Typography component="p" variant="h6" color="textSecondary">
        {model}
      </Typography>
    </React.Fragment>
  );
}

export default StarshipName;