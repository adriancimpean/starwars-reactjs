import swapi from "../api/swapi";
import * as types from "./types";


export const fetchStarships = () => async (dispatch) => {
  const response = await swapi.get("/starships/");

  dispatch({ type: types.FETCH_STARSHIPS, payload: response.data });
};

export const fetchStarship = (id) => async (dispatch) => {
  const response = await swapi.get(`/starships/${id}`);

  dispatch({ type: types.FETCH_STARSHIP, payload: response.data });
};

export const fetchPeople = () => async (dispatch) => {
  const response = await swapi.get("/people/");

  dispatch({ type: types.FETCH_PEOPLE, payload: response.data });
};

export const fetchCharacter = (id) => async (dispatch) => {
  const response = await swapi.get(`/people/${id}`);

  dispatch({ type: types.FETCH_CHARACTER, payload: response.data });
};

export const fetchPlanets = () => async (dispatch) => {
  const response = await swapi.get("/planets/");

  dispatch({ type: types.FETCH_PLANETS, payload: response.data });
};

export const fetchPlanet = (id) => async (dispatch) => {
  const response = await swapi.get(`/planets/${id}`);

  dispatch({ type: types.FETCH_PLANET, payload: response.data });
};

export const fetchFilmName = (url) => async (dispatch) => {
  const response = await swapi.get(`${url}`);

  dispatch({ type: types.FETCH_FILM_NAME, payload: response.data });
};

export const fetchFilm = (id) => async (dispatch) => {
  const response = await swapi.get(`/films/${id}`);

  dispatch({ type: types.FETCH_FILM, payload: response.data });
};
