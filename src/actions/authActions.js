import contactServer from "../api/contactServer";
import { LOGIN, REGISTER, SEND_MESSAGE } from "./types";

export const login = (user) => async (dispatch) => {
  const response = await contactServer.post("/login", user);
  dispatch({ type: LOGIN, payload: response.data });
};

export const register = (user) => async (dispatch) => {
  const response = await contactServer.post("/register", user);

  dispatch({ type: REGISTER, payload: response.data });
};

export const sendMessage = (message) => async (dispatch) => {
  const response = await contactServer.post("/contact", message);

  dispatch({ type: SEND_MESSAGE, payload: response.data });
};
