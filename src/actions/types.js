export const FETCH_STARSHIPS = "FETCH_STARSHIPS";
export const FETCH_STARSHIP = "FETCH_STARSHIP";

export const FETCH_PEOPLE = "FETCH_PEOPLE";
export const FETCH_CHARACTER = "FETCH_CHARACTER";

export const FETCH_PLANETS = "FETCH_PLANETS";
export const FETCH_PLANET = "FETCH_PLANET";

export const FETCH_FILM_NAME = "FETCH_FILM_NAME";
export const FETCH_FILM = "FETCH_FILM";

export const SEND_MESSAGE = "SEND_MESSAGE";

//auth
export const LOGIN = "LOGIN";
export const REGISTER = "REGISTER";
